// Real World Application Of Objects
let pokemon = ["Pikachu","Charizard","Squirtle","Bulbasaur"];
let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
    friends: {
        hoenn: ["May","Max"],
        kanto: ["Brock","Misty"]
    },
    talk: function() {
        console.log("Pikachu! I choose you!");
    }
};
    console.log(trainer);
    console.log('Result of dot notation: ' + '\n' + trainer.name);
    console.log('Result of square bracket notation:');
    console.log(["Pikachu","Charizard","Squirtle","Bulbasaur"]);
    console.log('Result of talk method');
    trainer.talk();

/*
    - Scenario
        1. We would like to create a game that would have several pokemon interact with each other
        2. Every pokemon would have the same set of stats, properties and functions
*/

function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack;
        console.log(target.name + "'s health is now reduced to " + target.health);
        };
    this.faint = function(target){
        if (target.health === 0) {
        console.log(this.name + 'fainted.');
        }
    }
};

// Creates new instances of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon('Pikachu', 12);
let geodude = new Pokemon('Geodude', 8);
let mewtwo = new Pokemon('Mewtwo', 100)

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(geodude);
console.log(geodude);
